***
## Developer Workflow
***

* [devdocs.io](http://devdocs.io/)
* [jstherightway.org](http://jstherightway.org/)
* [htmlcsstherightway.org](http://htmlcsstherightway.org/)
* [css optimization](https://css.github.io/csso/csso.html)
* [phptherightway.com](http://www.phptherightway.com/)
* [phpunit.de](https://phpunit.de/)
* [jsoneditoronline.org](http://jsoneditoronline.org/)
* [howtocenterincss.com](http://howtocenterincss.com/)
* [know-it-all.io](https://know-it-all.io/)

***
## Cross-Browser
***

* [browserstack.com](http://www.browserstack.com/)
* [caniuse.com](http://caniuse.com/)
* [shouldiprefix.com](http://shouldiprefix.com/)
* [html5please.com](http://html5please.com/)
* [modernizr.com](https://modernizr.com/)
* [normalizeCSS](http://necolas.github.io/normalize.css/)

***
## Image Optimization
***

* [imagify.io](http://imagify.io/)
* [optimizilla.com](http://optimizilla.com/)
* [compressnow.com](http://compressnow.com/)
* [tinypng.com](https://tinypng.com/)

***
## Website Tests
***

* Mobile friendly
    * [testmysite.thinkwithgoogle.com](https://testmysite.thinkwithgoogle.com/)
    * [https://search.google.com/search-console/mobile-friendly](https://search.google.com/search-console/mobile-friendly)

* Page Speed
    * [gtmetrix.com](http://gtmetrix.com/)
    * [tools.pingdom.com](https://tools.pingdom.com/)
    * [tools.keycdn.com](https://tools.keycdn.com/)

* SEO
    * [seositecheckup.com](http://seositecheckup.com/)

* Structed Data Schema
    * [schema.org](https://schema.org/docs/schemas.html)
    * [structured-data testing-tool](https://search.google.com/structured-data/testing-tool/u/0/)
    * Articles
        * [Introduction to Structured Data](https://developers.google.com/search/docs/guides/intro-structured-data)
        * [Schema.org Structured Data](https://moz.com/learn/seo/schema-structured-data)
        * [How to Boost Your SEO by Using Schema Markup](https://blog.kissmetrics.com/get-started-using-schema/)

* Other
    * [https://developers.facebook.com/tools/debug/](https://developers.facebook.com/tools/debug/)
    * [https://www.ssllabs.com/ssltest/](https://www.ssllabs.com/ssltest/)
    * [https://securityheaders.io/](https://securityheaders.io/)
