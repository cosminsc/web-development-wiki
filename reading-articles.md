***
## MISC
***

### ● [8 Ways to Become a Better Coder](https://blog.newrelic.com/2016/02/22/8-ways-become-a-better-coder/)


***
## CSS
***

### ● [BEM Methodology](http://getbem.com/)
### ● [Working with BEM at Scale — Advice From Top Developers](https://www.sitepoint.com/working-bem-scale-advice-top-developers/)
### ● [Battling BEM (Extended Edition): 10 Common Problems And How To Avoid Them](https://www.smashingmagazine.com/2016/06/battling-bem-extended-edition-common-problems-and-how-to-avoid-them/)
### ● [Font sizing with rem](https://snook.ca/archives/html_and_css/font-size-with-rem)
### ● [Scaling CSS Components with BEM, REMs, & EM](https://www.lullabot.com/articles/scaling-css-components-with-bem-rems-ems)
### ● [What’s the Difference Between Leading, Kerning and Tracking](https://creativemarket.com/blog/whats-the-difference-between-leading-kerning-and-tracking)
### ● [Use :not to Save Time and Lines of Code](https://theboldreport.net/2017/02/css-tip-use-not-to-save-time-and-lines-of-code/)
### ● [Design Your Content Typography First](https://css-tricks.com/design-content-typography-first-look-type-nugget/)

***
## JS
***

### ● [Leveling Up Your JavaScript](http://developer.telerik.com/featured/leveling-up-your-javascript/)