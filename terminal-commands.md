# Install PHPUnit

***Latest version***
```
wget https://phar.phpunit.de/phpunit.phar
chmod +x phpunit.phar
sudo mv phpunit.phar /usr/local/bin/phpunit
```

***Other version***
```
wget https://phar.phpunit.de/phpunit-x.x.x.phar
chmod +x phpunit-x.x.x.phar
sudo mv phpunit-x.x.x.phar /usr/local/bin/phpunit
```

***Uninstall***
```
sudo rm -r /usr/local/bin/phpunit
```

# Install Apache2
```
sudo apt-get install apache2
```

***Uninstall***
```
sudo apt-get purge apache2
sudo apt-get autoremove
```

# Insall PHP

### Add the PHP repository
To install PHP 7.3 you’ll need to use a third-party repository. We’ll use the repository by Ondřej Surý that we previously used.

First, make sure you have the following package installed so you can add repositories:
```
sudo apt-get install software-properties-common
```
Next, add the PHP repository from Ondřej:
```
sudo add-apt-repository ppa:ondrej/php
```
And finally, update your package list:
```
sudo apt-get update
```
### Install PHP7.3
```
sudo apt-get install php7.3
```

This command will install additional packages:

* libapache2-mod-php7.3
* libaprutil1-dbd-sqlite3
* php7.3-cli
* php7.3-common
* php7.3-json
* php7.3-opcache
* php7.3-readline
* …and others.

### Install modules
```
sudo apt-get install php-pear php7.3-curl php7.3-dev php7.3-gd php7.3-mbstring php7.3-zip php7.3-mysql php7.3-xml
```


# Install Composer
```
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
sudo apt install composer
```

# Install last version of Nodejs and NPM
```
wget -qO- https://deb.nodesource.com/setup_7.x | sudo bash -
sudo apt-get install -y nodejs
```


# Edit php.ini
```
sudo nano /etc/php5/apache2/php.ini
sudo service apache2 restart
```

# Adding subdomains to your localhost

```
#run:
sudo nano /etc/hosts
```

```
#add subdomain:
127.0.1.1       my_subdomain.localhost
```

```
#create a new apache config from the defatult one and name it same as your subdomain
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/my_subdomain.localhost.conf
```

```
#run:
sudo nano /etc/apache2/sites-available/my_subdomain.localhost.conf

#add ServerName & ServerAlias to the new config file without '#':
# ServerAlias my_subdomain.localhost
# ServerName my_subdomain.localhost
```

```
#enable new subdomain config:
sudo a2ensite my_subdomain.localhost.conf
```

```
#restart apache
sudo service apache2 restart
```

# Install WP_CLI
```
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar 
chmod +x wp-cli.phar 
sudo mv wp-cli.phar /usr/local/bin/wp

Run wp --info to confirm its installation.
```